$(function () {

	$('.carousel').carousel();

	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav',
		adaptiveHeight: true,
		lazyLoad: 'ondemand',
		autoplay: true,
		autoplaySpeed: 5000,
	});
	$('.slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		adaptiveHeight: true,
		lazyLoad: 'ondemand',
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 3
				}
			},{
				breakpoint: 500,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 2
				}
			}
		]
	});

	$("button#submit").click(function () {
		$.ajax({
			type: "POST",
			url: "process.php",
			data: $('form#contact').serialize(),
			success: function (msg) {
				$("#order").modal('hide');
			},
			error: function () {
				// alert("failure");
				$("#order").modal('hide');

			}
		});
	});

	$('a[href*="#"]').on('click', function (e) {
		e.preventDefault();
	
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top
		}, 500, 'linear');
	});

	$('.nav-link-main').on('click', function(){
		$('.navbar-collapse').collapse('hide');
	});
});
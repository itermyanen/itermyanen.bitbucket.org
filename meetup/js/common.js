$(function() {
	// Custom JS
	var austDay = new Date();
	austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 1);
	$('#defaultCountdown').countdown({until: austDay, padZeroes: true});

	$(".navbar-toggler").click(function() {  //use a class, since your ID gets mangled
		$(".menu").toggleClass("active");      //add the class to the clicked element
	});

});
